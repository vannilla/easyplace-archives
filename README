EasyPlace - archives submodule

This is the submodule of the EasyPlace project containing the software
that deals with versioned data.  In particular, it handles post and
pool version history.

Editing a post (e.g. adding tags) and pools (e.g. adding a new
picture) creates a new version which can be rolled back if incorrect
or if it's considered vandalism.

On the original Danbooru, saving versions was done by sending each
version to a queue handled by Amazon's web services, allowing the site
to process the editing without locking the whole thing, especially
when many users were working.

Since EasyPlace is designed for small boorus with only a handful of
users creating versions (either for posts or pools), there's no need
to go as far as relying on Amazon to handle a queue which would
normally be underused; as such, the queue is now a local service.
Check the `queue_processor.rb' file for further informations.

Also check EasyPlace's README to learn about the project as a whole.
Read LICENSE to learn about your rights.
