set :application, "archives"
set :repo_url, "https://gitlab.com/vannilla/easyplace-archives.git"
set :deploy_to, "/var/www/archives"
set :rbenv_type, :user
set :rbenv_ruby, "2.7.1"
set :linked_files, fetch(:linked_files, []).push(".env")
