=begin

Uncomment the following commands if you need to run something periodically.
Like cron, but handled by Rails or something like that.

=end

# every "0 1 * * *" do
# 	command "bash -l -c 'cd /var/www/archives/current && scripts/backup_db.sh'"
# end

# every "0 3 * * *" do
# 	command "bash -l -c 'cd /var/www/archives/current && scripts/prune_backup_dbs.sh'"
# end
