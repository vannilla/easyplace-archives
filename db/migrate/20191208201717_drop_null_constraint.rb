class DropNullConstraint < ActiveRecord::Migration[5.0]
  def change
    change_column_null :pool_versions, :booru_id, true
    change_column_null :post_versions, :booru_id, true
  end
end
