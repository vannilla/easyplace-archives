#!/usr/bin/env ruby

=begin

This program is a queue service for the Archives system.  If the
service is not available, Danbooru will not work at all, so make sure
it is started and especially make sure it is restarted after e.g. a
crash.

Because it has to be in execution as long as the booru is up, this
program was designed so that it can be potentially configured while
it's running, so as to avoid having to restart it (possibly placing
the whole site in maintenance mode.)  This feature is not currently
implemented, but it should be trivial to do if there is the necessity.

The following is a sample unit file for systemd.  Adapt it to your
needs.  In particular, make sure you change "User" and
"WorkingDirectory" if they ought to be different.

[Unit]
Description=The Archives queue processor for Danbooru

[Service]
Type=simple
User=archiver
Restart=always
WorkingDirectory=/var/www/archives/current
ExecStart=/bin/bash -lc 'bundle exec ruby services/queue_processor.rb'
TimeoutSec=30
RestartSec=15

[Install]
WantedBy=multi-user.target

=end

$:.unshift(File.expand_path("../../models", __FILE__))

require 'dotenv'
require 'logger'
require 'optparse'
require 'socket'
require 'pool_version'
require 'post_version'

Dotenv.load '.env', '/run/secrets/archives_env'

$options = {
  logfile: 'queue_processor.log',
  verbose: false,
  port: 8039,
  # How many bytes are read from the socket.
  # It's an arbitrary number and it's configurable if it needs to be bigger
  # (or smaller, but I don't see why it should be smaller.)
  bufsize: 8192,
}

def print_help
  puts 'Available options:'
  puts '     --logfile=LOGFILE    log events to LOGFILE'
  puts '     --verbose            print a lot of messages'
  puts '     --port=PORT          use port number PORT'
  puts '     --buffer=SIZE        read messages within a buffer of SIZE bytes'
  puts '     --help               print this message'
end

OptionParser.new do |opts|
  opts.on('--logfile=LOGFILE', String) do |logfile|
    $options[:logfile] = logfile
  end
  opts.on('--verbose') do
    $options[:verbose] = true
  end
  opts.on('--port=PORT', Integer) do |port|
    $options[:port] = port.abs()
  end
  opts.on('--buffer=SIZE', Integer) do |size|
    $options[:bufsize] = size.abs()
  end
  opts.on('--help') do
    print_help
    exit
  end
end.parse!

# This class exists to make it possible to change the log file while
# the service is running.
class QueueLogger
  def initialize
    if $options[:logfile] == 'stdout'
      @isstd = true
      @file = STDOUT
    else
      @isstd = false
      @file = File.open($options[:logfile], 'a')
    end
    @fname = $options[:logfile]
    @file.sync = true
    @logger = Logger.new(@file, 0)
  end

  def change_file(filename)
    @file.close unless @isstd == true
    if filename != @fname
      @fname = filename
      @logger.close
      if filename == 'stdout'
        @file = STDOUT
        @isstd = true
      else
        @file = File.open(filename, 'a+')
        @isstd = false
      end
      @file.sync = true
      @logger = Logger.new(@file, 0)
    end
  end

  def info(msg)
    @logger.info(msg)
  end

  def warn(msg)
    @logger.warn(msg)
  end

  def error(msg)
    @logger.error(msg)
  end

  def fatal(msg)
    @logger.fatal(msg)
  end
end

$logger = QueueLogger.new
$queue = Queue.new
$server = TCPServer.new('localhost', $options[:port])

# These traps probably don't work, I can't tell.
# Sorry.
Signal.trap('INT') do
  $queue.close
end

Signal.trap('TERM') do
  $queue.close
end

if $options[:verbose]
  $logger.info("Port #{$options[:port]}; buffer #{$options[:bufsize]} bytes")
  $logger.info('Starting...')
end

# This is the executing thread.
# Messages are read from the queue and are operated on accordingly.
Thread.start do
  while true
    begin
      if $queue.closed?
        Thread.exit
      end

      msg = $queue.pop
      if msg.nil?
        $logger.info('No message available, what could it mean?') if $options[:verbose]
        # It probably should shutdown here, but it seems that simply
        # terminating causes problems, so let's just enter an infinite
        # loop until the process manager forcibly kills the thing.
        next
      end

      cmd, json = msg.split(/\n/)
      if cmd.nil?
        $logger.warn('No command specified') if $options[:verbose]
        next
      end

      json = JSON.parse(json)

      case cmd
      when 'add pool version'
        $logger.info("add pool version json=#{json.inspect}") if $options[:verbose]
        PoolVersion.create_from_json(json)
      when 'add post version'
        $logger.info("add post version json=#{json.inspect}") if $options[:verbose]
        PostVersion.create_from_json(json)
      when 'change logfile'
        $logger.info("logging to #{json['filename']}")
        $logger.change_file(json['filename'])
      else
        $logger.info("Unknown command #{cmd}")
      end
    rescue Exception => e
      $logger.error("#{e.class} - #{e.message}")
      e.backtrace.each do |line|
        $logger.error(line)
      end
    end
  end
end

# Each thread created in this loop obtains a message from a client
# connecting to this service.
while true
  Thread.start($server.accept) do |client|
    if $queue.closed?
      client.close
      Thread.exit
    end

    msg = client.recv($options[:bufsize])
    client.close

    if msg.nil?
      $logger.warn('No message from client') if $options[:verbose]
      Thread.exit
    end

    begin
      $queue.push(msg)
    rescue IOError
      $logger.error('IO Error!')
      Thread.exit
    rescue ClosedQueueError
      Thread.exit
    end
  end
end

=begin
Old code; please don't mind it.

Signal.trap("TERM") do
  $running = false
  QUEUE.close
end

$thread1 = Thread.new do
  LOGGER.info('Started connection thread') if $options[:verbose]
  
  run = true
  MUTEX.synchronize {
    run = $running
  }
  
  while run
    begin
      MUTEX.synchronize {
        run = $running
      }

      if run == false
        QUEUE.close
        Thread.exit
      end
      
      con = SERVER.accept
      if con == nil
        QUEUE.close
        MUTEX.synchronize {
          $running = false
        }
        Thread.exit
      end
      
      msg = con.recv($options[:bufsize])
      con.close
      if msg == nil
        QUEUE.close
        MUTEX.synchronize {
          $running = false
        }
        Thread.exit
      end

      QUEUE.push(msg) unless QUEUE.closed?
    rescue IOError
      LOGGER.info("Closing due to IOError") if $options[:verbose]
      QUEUE.close
      MUTEX.synchronize {
        $running = false
        Thread.exit
      }
    rescue Exception => e
      LOGGER.error("#{e.class} thrown with message #{e.message}")
      60.times do
        sleep 1
        MUTEX.synchronize {
          run = $running
        }
        if run == false
          QUEUE.close
          Thread.exit
        end
      end
      retry
    end
  end
  LOGGER.info('Connection thread terminating normally') if $options[:verbose]
end

$thread2 = Thread.new do
  LOGGER.info('Started consumer thread') if $options[:verbose]

  run = true
  MUTEX.synchronize {
    run = $running
  }

  while run
    begin
      MUTEX.synchronize {
        run = $running
      }

      if run == false
        QUEUE.close
        Thread.exit
      end

      Thread.exit if QUEUE.closed?

      msg = QUEUE.pop
      if msg == nil
        next
      end

      cmd, json = msg.split(/\n/)
      if cmd == nil
        next
      end

      json = JSON.parse(json)

      case cmd
      when "add pool version"
        LOGGER.info("add pool version json=#{json.inspect}")
        PoolVersion.create_from_json(json)
      when "add post version"
        LOGGER.info("add post version json=#{json.inspect}")
        PostVersion.create_from_json(json)
      else
        LOGGER.info("Unknown command #{cmd}")
      end
    end
  end
  
  LOGGER.info('Consumer thread terminating normally') if $options[:verbose]
end

LOGGER.info('Waiting to join threads') if $options[:verbose]

begin
  $thread1.join
  $thread2.join
rescue Interrupt
  MUTEX.synchronize {
    $running = false
  }
  QUEUE.close
  LOGGER.info('Interrupted') if $options[:verbose]
end

LOGGER.info('Closing') if $options[:verbose]
SERVER.close
=end
